package ru.t1.aksenova.tm.service.dto;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.aksenova.tm.api.repository.dto.ISessionDTORepository;
import ru.t1.aksenova.tm.api.service.dto.ISessionDTOService;
import ru.t1.aksenova.tm.dto.model.SessionDTO;
import ru.t1.aksenova.tm.enumerated.Role;
import ru.t1.aksenova.tm.exception.entity.SessionNotFoundException;
import ru.t1.aksenova.tm.exception.field.DateEmptyException;
import ru.t1.aksenova.tm.exception.field.IdEmptyException;
import ru.t1.aksenova.tm.exception.field.UserIdEmptyException;
import ru.t1.aksenova.tm.exception.user.RoleEmptyException;

import java.util.Date;
import java.util.List;

@Service
@AllArgsConstructor
public class SessionDTOService extends AbstractUserOwnedDTOService<SessionDTO, ISessionDTORepository>
        implements ISessionDTOService {

    @Nullable
    @Autowired
    private ISessionDTORepository repository;

    @NotNull
    @Override
    public ISessionDTORepository getRepository() {
        return repository;
    }

    @NotNull
    @Override
    public SessionDTO create(
            @Nullable final String userId,
            @Nullable final String role
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (role == null || role.isEmpty()) throw new RoleEmptyException();
        @NotNull SessionDTO session = new SessionDTO();
        session.setUserId(userId);
        session.setRole(Role.valueOf(role));
        add(session);
        return session;
    }

    @NotNull
    @Override
    public List<SessionDTO> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return getRepository().findAll(userId);
    }

    @Nullable
    @Override
    public SessionDTO findOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final SessionDTO session = getRepository().findOneById(userId, id);
        if (session == null) throw new SessionNotFoundException();
        return session;
    }

    @Override
    @Transactional
    public void clear() {
        getRepository().clear();
    }

    @Override
    @Transactional
    public void removeAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        getRepository().clear(userId);
    }

    @Nullable
    @Override
    public SessionDTO removeOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final SessionDTO session = findOneById(userId, id);
        remove(userId, session);
        return session;
    }

    @NotNull
    @Override
    public SessionDTO updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String role,
            @Nullable final Date date
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (role == null || role.isEmpty()) throw new RoleEmptyException();
        if (date == null) throw new DateEmptyException();
        @Nullable final SessionDTO session = findOneById(userId, id);
        if (session == null) throw new SessionNotFoundException();
        session.setRole(Role.valueOf(role));
        session.setDate(date);
        update(userId, session);
        return session;
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return (getRepository().findOneById(userId, id) != null);
    }

    @Override
    public long getSize(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return getRepository().getCount(userId);
    }

}
