package ru.t1.aksenova.tm.dto.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aksenova.tm.enumerated.Role;

import javax.persistence.*;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm_user")
@JsonIgnoreProperties(ignoreUnknown = true)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class UserDTO extends AbstractModelDTO {

    private static final long serialVersionUID = 1;

    @Nullable
    @Column(length = 50, nullable = false)
    private String login;

    @Nullable
    @Column(length = 100, name = "password_hash", nullable = false)
    private String passwordHash;

    @Nullable
    @Column(length = 50)
    private String email;

    @Nullable
    @Column(length = 100, name = "first_name")
    private String firstName;

    @Nullable
    @Column(length = 100, name = "last_name")
    private String lastName;

    @Nullable
    @Column(length = 100, name = "middle_name")
    private String middleName;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(length = 50, nullable = false)
    private Role role = Role.USUAL;

    @Column
    private boolean locked = false;

}
