package ru.t1.aksenova.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractResultResponse extends AbstractResponse {

    @NotNull
    private boolean success = true;

    @NotNull
    private String message = "";

    public AbstractResultResponse(@NotNull final Throwable error) {
        setSuccess(false);
        setMessage(error.getMessage());
    }

}
