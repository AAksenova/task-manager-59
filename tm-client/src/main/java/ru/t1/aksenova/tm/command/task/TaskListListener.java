package ru.t1.aksenova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.aksenova.tm.dto.model.TaskDTO;
import ru.t1.aksenova.tm.dto.request.TaskListRequest;
import ru.t1.aksenova.tm.dto.response.TaskListResponse;
import ru.t1.aksenova.tm.enumerated.TaskSort;
import ru.t1.aksenova.tm.event.ConsoleEvent;
import ru.t1.aksenova.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Component
public final class TaskListListener extends AbstractTaskListener {

    @NotNull
    public static final String NAME = "task-list";

    @NotNull
    public static final String DESCRIPTION = "Show list tasks.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    @EventListener(condition = "@taskListListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[TASK LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(TaskSort.values()));
        @NotNull String sortType = TerminalUtil.nextLine();
        if (sortType.isEmpty()) {
            sortType = "BY_CREATED";
            System.out.println("DEFAULT SORT: BY_CREATED");
        }

        @Nullable final TaskListRequest request = new TaskListRequest(getToken());
        request.setSort(sortType);
        @Nullable final TaskListResponse response = getTaskEndpoint().listTask(request);
        if (response.getTasks() == null) response.setTasks(Collections.emptyList());
        @Nullable final List<TaskDTO> tasks = response.getTasks();

        int index = 1;
        for (@Nullable final TaskDTO task : tasks) {
            if (task == null) continue;
            System.out.println(index + ". " + task.getName() + ": " + task.getDescription() + " ID: " + task.getId());
            index++;
        }
    }

}
