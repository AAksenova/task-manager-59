package ru.t1.aksenova.tm.service;

import com.jcabi.manifests.Manifests;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import ru.t1.aksenova.tm.api.service.IPropertyService;

@Getter
@Service
@PropertySource("classpath:application.properties")
public final class PropertyService implements IPropertyService {

    @NotNull
    public static final String APPLICATION_VERSION_KEY = "buildNumber";
    @NotNull
    public static final String AUTHOR_NAME_KEY = "developer";
    @NotNull
    public static final String AUTHOR_EMAIL_KEY = "email";
    @NotNull
    public static final String EMPTY_VALUE = "---";
    @NotNull
    public static final String GIT_BRANCH = "gitBranch";
    @NotNull
    public static final String GIT_COMMIT_ID = "gitCommitId";
    @NotNull
    public static final String GIT_COMMIT_TIME = "gitCommitTime";
    @NotNull
    public static final String GIT_COMMIT_MESSAGE = "gitCommitMessage";
    @NotNull
    public static final String GIT_COMMITTER_NAME = "gitCommitterName";
    @NotNull
    public static final String GIT_COMMITTER_NAME1 = "gitCommitterName";
    @Value("#{environment['application.version']}")
    private String applicationVersion;
    @Value("#{environment['server.port']}")
    private String port;
    @Value("#{environment['server.host']}")
    private String host;

    @NotNull
    @Override
    public String getAuthorName() {
        return "Anastasiya Aksenova";
    }

    @NotNull
    @Override
    public String getAuthorEmail() {
        return "aaksenova@t1-consulting.ru";
    }

    @NotNull
    @Override
    public String getGitBranch() {
        return read(GIT_BRANCH);
    }

    @NotNull
    @Override
    public String getGitCommitId() {
        return read(GIT_COMMIT_ID);
    }

    @NotNull
    @Override
    public String getGitCommitTime() {
        return read(GIT_COMMIT_TIME);
    }

    @NotNull
    @Override
    public String getGitCommitMessage() {
        return read(GIT_COMMIT_MESSAGE);
    }

    @NotNull
    @Override
    public String getGitCommitterName() {
        return read(GIT_COMMITTER_NAME);
    }

    @NotNull
    @Override
    public String getGitCommitterEmail() {
        return read(GIT_COMMITTER_NAME1);
    }

    @NotNull
    private String read(@Nullable final String key) {
        if (key == null || key.isEmpty()) return EMPTY_VALUE;
        if (!Manifests.exists(key)) return EMPTY_VALUE;
        return Manifests.read(key);
    }

}

