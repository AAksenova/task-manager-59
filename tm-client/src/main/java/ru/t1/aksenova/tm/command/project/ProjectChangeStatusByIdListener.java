package ru.t1.aksenova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.aksenova.tm.dto.request.ProjectChangeStatusByIdRequest;
import ru.t1.aksenova.tm.enumerated.Status;
import ru.t1.aksenova.tm.event.ConsoleEvent;
import ru.t1.aksenova.tm.util.TerminalUtil;

import java.util.Arrays;

@Component
public final class ProjectChangeStatusByIdListener extends AbstractProjectListener {

    @NotNull
    public static final String NAME = "project-change-status-by-id";

    @NotNull
    public static final String DESCRIPTION = "Change project status by id.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    @EventListener(condition = "@projectChangeStatusByIdListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {

        System.out.println("[CHANGE PROJECT STATUS BY ID]");
        System.out.println("ENTER PROJECT ID:");
        @NotNull final String id = TerminalUtil.nextLine();

        System.out.println("ENTER PROJECT STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        @NotNull final String statusValue = TerminalUtil.nextLine();

        @Nullable final ProjectChangeStatusByIdRequest request = new ProjectChangeStatusByIdRequest(getToken());
        request.setId(id);
        request.setStatus(statusValue);
        getProjectEndpoint().changeProjectStatusById(request);
    }

}
