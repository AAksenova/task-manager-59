package ru.t1.aksenova.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.aksenova.tm.dto.request.UserChangePasswordRequest;
import ru.t1.aksenova.tm.enumerated.Role;
import ru.t1.aksenova.tm.event.ConsoleEvent;
import ru.t1.aksenova.tm.util.TerminalUtil;

@Component
public final class UserChangePasswordListener extends AbstractUserListener {

    @NotNull
    public static final String NAME = "change-user-password";

    @NotNull
    public static final String DESCRIPTION = "Change password of current user.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    @EventListener(condition = "@userChangePasswordListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[CHANGE USER PASSWORD]");
        System.out.println("ENTER NEW PASSWORD:");
        @NotNull final String password = TerminalUtil.nextLine();

        @Nullable final UserChangePasswordRequest request = new UserChangePasswordRequest(getToken());
        request.setPassword(password);
        getUserEndpoint().changeUserPassword(request);
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
