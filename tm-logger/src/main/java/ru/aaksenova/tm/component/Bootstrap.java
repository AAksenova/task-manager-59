package ru.aaksenova.tm.component;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.aaksenova.tm.listener.EntityListener;

import javax.jms.*;

@Component
public final class Bootstrap {

    @NotNull
    private static final String QUEUE = "LOGGER";
    @NotNull
    @Autowired
    private EntityListener entityListener;
    @NotNull
    @Autowired
    private ConnectionFactory connectionFactory;

    @SneakyThrows
    public void start() {
        final Connection connection = connectionFactory.createConnection();
        connection.start();
        final Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        final Queue destination = session.createQueue(QUEUE);
        final MessageConsumer messageConsumer = session.createConsumer(destination);
        messageConsumer.setMessageListener(entityListener);
    }


}
